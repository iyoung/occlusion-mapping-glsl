#pragma once
#include "rt3d.h"
#include <string>
enum ElementType
{
	COLOUR_MAP,
	NORMAL_MAP,
	SPECULAR_MAP,
	HEIGHT_MAP,
	CHILD_GRAPHIC,
	OBJ_MODEL,
	MD2_MODEL,
};
class GraphicsObject
{
public:
	GraphicsObject(void);
	void setElement(ElementType type, std::string p_FileName);
	void draw()
	virtual ~GraphicsObject(void);
};

