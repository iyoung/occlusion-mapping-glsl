// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emissive;
	float shininess;
};
uniform mat4 view;
uniform lightStruct light;
uniform materialStruct material;
//texture information
uniform sampler2D colourMap;
uniform sampler2D normalMap;
uniform sampler2D specularMap;
uniform sampler2D displacementMap;
in vec3 ex_V;
in vec3 position;
in vec3 ex_L;
in vec3 ex_N;
in vec2 ex_TexCoord;
in mat3 surfaceToWorldMatrix;
//final fragment colour
layout(location = 0) out vec4 out_Color;
 
void main(void) {
		//this is just for debugging, increase this to reduce diffuse and specular attenuation over distance
		float scale = 2.0f;
		float dist = length(ex_L);
		//get our height from the displacement map
		float height = texture2D(displacementMap, ex_TexCoord.st).r;
		float v = height*-0.018+0.009;//height * scale + bias. Ideally scale & bias would be sent as uniform at rendering time
		//calculate our half vector, which is halfway between the light and view vector, transformed to tangent space
		vec3 halfVec = surfaceToWorldMatrix*normalize(ex_L + ex_V);
		//calculate the pixel offset and use that for all future texture lookups
		vec2 newCoords = ex_TexCoord+(halfVec.xy*v);
	    //get normal data from normal map texture, at new coordinates
        vec3 encNormal = texture2D(normalMap, newCoords).rgb*2.0f-1.0f;
        //and normalize
        vec3 NN = normalize(encNormal);
	
        //normalize light vector
        vec3 LL = normalize(ex_L);
        // Ambient intensity
        vec4 ambientI = light.ambient * material.ambient;
		
        // Diffuse intensity
        float diffuse = max(dot(NN,ex_L),0);
		if(diffuse>0.0)
		{
			vec4 diffuseI = ((light.diffuse * material.diffuse)*diffuse)/(dist/scale);

			// Calculate R - reflection of light
			vec3 R = normalize(reflect(-LL,NN));
			// Specular intensity
			float specMap = texture2D(specularMap, newCoords).r;
			//calculate attenuation
			float attenuation =1.0f/(dist/scale)*(dist/scale);
			//calculate specular component
			float specular = pow(max(dot(NN,halfVec),0.0),material.shininess);
			vec4 specularI = ((((material.specular*light.specular)*specular)*specMap)*attenuation);
			// final Fragment colour
			// there will be a final part here, in which we calculate attenuation as distance increases from the light source.
			if(material.emissive.w>0)
			{
					//unrelated to normal mapping, ignores lighting if an emissive material is present
					gl_FragColor = texture2D(colourMap, newCoords).rgba;
			}

			else
			{
					//now calculate the final fragment colour
					gl_FragColor =(ambientI + diffuseI + specularI)* texture2D(colourMap, newCoords).rgba;
			}
		}
		else gl_FragColor = vec4(vec3(0.0),1.0);
}