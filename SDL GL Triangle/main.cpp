// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// This program will render two triangles
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include "OBJFileLoader.h"
#include "rt3dObjLoader.h"
#include <ctime>
#include <SDL.h>
using namespace std;

// Globals
// Real programs don't use globals :-D
// Data would normally be read from files
std::stack<glm::mat4> mvStack;
GLfloat rotY,rotX;
GLfloat f;
GLfloat rot=50.0f;
GLfloat savedRot;
bool paused;
int xpos, ypos;
// position
glm::vec3 position = glm::vec3( 0, 0,0 );
// horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// vertical angle : 0, look at the horizon
float verticalAngle = 0.0f;
// roll angle
float rollAngle=0.0f;
// Initial Field of View
float initialFoV = 45.0f;
glm::vec2 mouse;
float speed = 5.0f; // 3 units / second
float mouseSpeed = 0.05f;
float scrWidth = 800.0f;
float scrHeight = 600.0f;

rt3d::lightStruct light0 = {
	{0.1f, 0.1f, 0.1f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
	{0.95f, 0.95f, 0.95f, 1.0f}, // specular
	{0.0f, 0.0f, -1.0f, 0.0f}  // position
};
rt3d::materialStruct material0 = {
	{0.1f, 0.1f, 0.1f, 1.0f},	// ambient
	{0.25f, 0.25f, 0.25f, 1.0}, // diffuse
	{0.1f, 0.1f, 0.1f, 0.1f},	// specular
	{0.0f, 0.0f, 0.0f, 0.0f	},	//emissive
	1.0f  // shininess

};
rt3d::materialStruct material1 = {
	{0.1f,0.1f,0.1f,1.0f},	//ambient
	{0.5f,0.5f,0.5f,1.0f},		//diffuse
	{0.9f,0.9f,0.9f,1.0f},		//specular
	{0.0f,0.0f,0.0f,0.0f},		// emissive
	40.0f						//shininess
};
rt3d::materialStruct material2 = {
	{0.2f, 0.2f, 0.4f, 1.0f},	// ambient
	{0.5f, 0.5f, 0.8f, 1.0f},	// diffuse
	{0.8f, 0.8f, 1.0f, 1.0f},	// specular
	{0.0f,0.0f,0.0f,0.0f},		// emissive
	1.0f  // shininess

};

int modeSelector;
GLuint cubeVertCount;
GLuint cubeIndexCount;
clock_t timer;
clock_t now;
clock_t deltaTime;
std::vector<GLfloat> cubeVerts;
std::vector<GLfloat> cubeNormals;
std::vector<GLuint> cubeIndices;
std::vector<GLfloat> cubeTangents;
glm::vec3 rightD;
glm::vec3 directionD;
glm::vec4 lightPos;
GLuint textures[4];
std::vector<GLfloat> texCoords;
glm::vec3 up;
GLuint shaderProgram;

GLuint meshObjects[2];
float dx,dy,dz;
glm::mat4 viewMatrix;

// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        scrWidth, scrHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

	return window;
}


void init(void) {
	// For this simple example we'll be using the most basic of shader programs
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	timer = clock();
	now = timer;
	rt3d::loadObj("cylinder.obj",cubeVerts,cubeNormals,texCoords,cubeIndices);

	//load up our shaders and textures
	shaderProgram=rt3d::initShaders("phong-tex-occlusion.vert","phong-tex-occlusion.frag");
	textures[0] = rt3d::loadBitmap("brick2_diffuse.bmp");
	textures[1] = rt3d::loadBitmap("brick2_normal.bmp");
	textures[2] = rt3d::loadBitmap("brick2_specular.bmp");
	textures[3] = rt3d::loadBitmap("brick2_displacement.bmp");
	f = 60.0f;
	// Going to create our mesh objects here
	cubeVertCount= cubeIndices.size();
	std::vector<glm::vec3> iNormals, iVerts;
	std::vector<glm::vec2> iTexCoords;
	rt3d::calculateBitangents(cubeVerts,cubeIndices,cubeTangents,texCoords, cubeNormals);
	meshObjects[0] = rt3d::createMeshWithTans(cubeVertCount,cubeVerts.data(),cubeTangents.data(),cubeNormals.data(),texCoords.data(),cubeVertCount,cubeIndices.data());
	meshObjects[0] = rt3d::createMesh(cubeVertCount,cubeVerts.data(),nullptr,cubeNormals.data(),texCoords.data(),cubeVertCount,cubeIndices.data());
	position = glm::vec3(0, 0, 0.0f);
	rt3d::setLight(shaderProgram,light0); 
		//this next part gets our uniform locations for colour(diffuse),normal and specular maps
		glActiveTexture(GL_TEXTURE0);
		int textureLocation = glGetUniformLocation(shaderProgram,"colourMap");
		glUniform1i(textureLocation,0);
		glBindTexture(GL_TEXTURE_2D,textures[0]);
		glActiveTexture(GL_TEXTURE1);
		int normalLocation = glGetUniformLocation(shaderProgram,"normalMap");
		glUniform1i(normalLocation,1);
		glBindTexture(GL_TEXTURE_2D,textures[1]);
		glActiveTexture(GL_TEXTURE2);
		int specLocation = glGetUniformLocation(shaderProgram,"specularMap");
		glUniform1i(specLocation,2);
		glBindTexture(GL_TEXTURE_2D,textures[2]);
		glActiveTexture(GL_TEXTURE3);
		int dispLocation = glGetUniformLocation(shaderProgram,"displacementMap");
		glUniform1i(dispLocation,3);
		glBindTexture(GL_TEXTURE_2D,textures[3]);
}
bool handleEvents() 
{

	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_ESCAPE]) return false;
	if ( keys[SDL_SCANCODE_A] ) horizontalAngle += 1.0*((float)deltaTime)/1000;
	if ( keys[SDL_SCANCODE_D] ) horizontalAngle -= 1.0*((float)deltaTime)/1000;
	if ( keys[SDL_SCANCODE_Q] ) rollAngle += 1.0*((float)deltaTime)/1000;
	if ( keys[SDL_SCANCODE_E] ) rollAngle -= 1.0*((float)deltaTime)/1000;
	if ( keys[SDL_SCANCODE_W] ) verticalAngle += 1.0*((float)deltaTime)/1000;
	if ( keys[SDL_SCANCODE_S] )	verticalAngle -= 1.0*((float)deltaTime)/1000;
	//calculate direction (forward orientation) and right ascension
	directionD = glm::vec3(	cos(verticalAngle) * sin(horizontalAngle),
							sin(verticalAngle),
							cos(verticalAngle) * cos(horizontalAngle));
	// Right vector
	rightD = glm::vec3(	sin(horizontalAngle - 3.14f/2.0f),
						0,
						cos(horizontalAngle - 3.14f/2.0f));
	
	up = glm::cross( rightD, directionD );
	if ( keys[SDL_SCANCODE_LEFT] ) position -= rightD * speed*(((float)deltaTime)/1000);;
	if ( keys[SDL_SCANCODE_RIGHT] ) position += rightD * speed*(((float)deltaTime)/1000);;
	if ( keys[SDL_SCANCODE_UP] ) position += up * speed*(((float)deltaTime)/1000);;
	if ( keys[SDL_SCANCODE_DOWN] ) position -= up * speed*(((float)deltaTime)/1000);;
	if ( keys[SDL_SCANCODE_TAB] ) position += directionD * speed*(((float)deltaTime)/1000);
	if ( keys[SDL_SCANCODE_SPACE] ) position -= directionD * speed*(((float)deltaTime)/1000);
	if ( keys[SDL_SCANCODE_0] ) modeSelector = 0;
	if ( keys[SDL_SCANCODE_1] ) modeSelector = 1;
	if ( keys[SDL_SCANCODE_P] ) rot = 0;
	if ( keys[SDL_SCANCODE_O] ) rot = 10;
	
	
	return true;
}
void updateModel()
{
	now = clock();
	deltaTime = now-timer;
}
void draw(SDL_Window * window) {
	
	if(deltaTime>=(16/1000))
	{
		/*printf(" mspf:%f \n",deltaTime);*/
		if(!paused)

		// clear the screen buffer and depth buffer
		glClearColor(0.2f,0.0f,0.0f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		
		//set and create view matrix for modelView calculations
		viewMatrix       = glm::lookAt(
		position,				// Camera is here
		position+directionD,	// and looks here : at the same position, plus "direction"
		up						// Head is up (set to 0,-1,0 to look upside-down)
		);
		//set up projection matrix and send to shader program
		glm::mat4 projection(1.0);
		projection = glm::perspective(f,scrWidth/scrHeight,0.05f,120.0f);
		rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));

		//set up identity matrix and use this to set lighting position
		glm::mat4 identity(1.0);

		mvStack.push(identity); //push #1: ident matrix to stack

		mvStack.top() = glm::translate(mvStack.top(),glm::vec3(0.0,0.0,-5.0));
		mvStack.top() = glm::rotate(mvStack.top(),(rot/1000)*now,glm::vec3(0.0,1.0,0.0));
		mvStack.push(mvStack.top());
		//transform by camera coords
		//set light & material for correct shading
		rt3d::setEmissiveMaterial(shaderProgram,material1);
		//perform transformations
		glm::mat4 modelView = viewMatrix*mvStack.top();
		glm::vec4 tmp =	viewMatrix*glm::vec4(light0.position[0],light0.position[1],light0.position[2],light0.position[3]);
		glm::mat3 inverseModelView = glm::transpose(glm::inverse(glm::mat3(modelView)));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(tmp));
		glm::vec3 eyePos = glm::vec3(viewMatrix*glm::vec4(position,1.0));
		//and set shader uniforms
		rt3d::setUniformMatrix4fv(shaderProgram, "model", glm::value_ptr(mvStack.top()));
		rt3d::setUniformMatrix4fv(shaderProgram, "view", glm::value_ptr(viewMatrix));
		rt3d::setUniformVector3f(shaderProgram, "eyePosition",glm::value_ptr(eyePos));
		rt3d::setUniformMatrix3fv(shaderProgram, "inverseMV", glm::value_ptr(inverseModelView));
		rt3d::setLightPos(shaderProgram,glm::value_ptr(tmp));
		//send final command to draw the mesh
		glEnable(GL_TRIANGLES);
		rt3d::drawIndexedMesh(meshObjects[0],cubeVertCount,GL_TRIANGLES);
		mvStack.pop(); //empty stack. rendering cycle complete
		SDL_GL_SwapWindow(window); // swap buffers
		timer = now;
	}
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;
	//initialize variables
	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
			{
				running = handleEvents();
			}
		}
		updateModel();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}