// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L vectors for use in fragment shader, phong-tex.frag
#version 330

//per vertex inputs stored on GPU memory
in vec3 in_Position;
in vec3 in_Normal;
in vec3 tangent;
//per mesh data sent at rendering time
uniform mat4 model; //object to world space transformations
uniform mat4 view;	//world to eye space transformations
uniform mat4 projection; //eye space to screen space
uniform mat3 inverseMV; //for transforming normals in 
uniform vec4 lightPosition;
uniform vec3 eyePosition;
//outputs to fragment shader
out vec3 position; //vertex position in eye space
out vec3 ex_L; //light vector
out vec3 ex_V; //view direction
out vec3 ex_N;
out mat3 surfaceToWorldMatrix; //TBN matrix for tangent/world transformations
in vec2 in_TexCoord;
out vec2 ex_TexCoord;
// multiply each vertex position by the MVP matrix
// and find V, L, and TBN Matrix vectors for the fragment shader
void main(void) 
{

		//calculate full transform matrix
        mat4 MVP = projection*view*model;
		mat4 modelview = view*model;
        //calculate the new projection space coordinate
        position = vec3(modelview*vec4(in_Position,1.0));
        gl_Position = MVP * vec4(in_Position,1.0);
		mat3 normalMatrix = mat3(inverse(modelview));
		normalMatrix = transpose(normalMatrix);
        //transform our normal by upper 3x3 modelview for lighting calculations
        vec3 transformedNormal = (normalMatrix * in_Normal);
        //transform tangent by model matrix, into world coordinates for calculating TBN matrix
        vec3 transformedTangent = (normalMatrix * tangent);
        //calculate TBN matrix, for transforming our texture normals from tangent space to world space
        //These are the basis vectors of texture space
        //first, precalculated tangent axis
		vec3 Bitangent = normalize(cross(transformedNormal,transformedTangent));
        surfaceToWorldMatrix = mat3(transformedTangent, Bitangent, transformedNormal);
         //no changes to texcoords, they are needed in fragment shader
        ex_TexCoord = in_TexCoord;
        //calculate vertex/light vector in tangent space
        ex_L = vec3(lightPosition.xyz-position);
		ex_N = transformedNormal;
        //calculate camera position
        vec4 viewP = vec4(eyePosition,1.0);
        //calculate vertex/camera vector
        ex_V = (-position);

}